# Gyár (factory) tervezési minta

## Ellenőrző kérdések

* Hogy biztosítja a minta azt, hogy ne kelljen a kliens módosítani egy új termék
bevezetése esetén?
* A létrehozó metódusok miért nem `static` minősítésűek?
* A konkrét gyár honnan tudja, hogy melyik terméket kell létrehoznia?

## Állatok létrehozása 

Hozzunk létre egy `IAnimal` interfészt, melyben van egy `speak` metódus, melynek nincs paramétere,
 visszatérési értéke pedig az adott állat által keltett hang utánzó szó. (`string speak()`)

Implementáljuk az előző interfészt egy `Cock` osztályban úgy, hogy az 
adott metódus visszatérési értéke "kukuriku" legyen!

Implementáljuk az előző interfészt egy `Horse` osztályban úgy, hogy az 
adott metódus visszatérési értéke "nyihaha" legyen!

Implementáljuk az előző interfészt egy `Frog` osztályban úgy, hogy az 
adott metódus visszatérési értéke "brekeke" legyen!

Készítsünk egy `IAnimalFactory` interfészt. Tartalmazzon egy `create` metódust,
 amely képes létrehozni egy állatot! `string` típusú paramétere van, amely az állat angol neve (`IAnimal *create(ctring animalName)`).
 
Készítsük el a konkrét gyárat `AnimalFactory` néven, melynek `sreate` metódusa
három féle paramétert ismer: "cock", "horse", "frog". A paraméter alapján létrehozza a 
megfelelő objektumot, és visszaadja.

Factory használatára hozzunk létre egy `Farm` osztályt, amely függősége az `IAnimalFactory` egy referenciája.

* Az osztály nyilván tartja, hogy milyen állat van a farmon egy `_animals` nevű listában (`list<IAnimal *>`).
* Hozz létre egy `void newAnimalArrived(string animalName)` metódust, amely eltárolja a listában a jövevényt. (Ismeretlen állat esetén nem tárol semmit.)
* Hozz létre egy `list<string> getAnimalVoices()` metódust, amely visszatér a farmon található állatok hangjával.
Egy hang csak egyszer szerepeljen.





