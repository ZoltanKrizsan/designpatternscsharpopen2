cmake_minimum_required(VERSION 3.6)
project(designpatternscsharpopen2)
# We need thread support
find_package(Threads REQUIRED)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp)

#add_subdirectory(command)
add_subdirectory(state)
add_subdirectory(xTernal/gtest)
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})
include_directories(${gmock_SOURCE_DIR}/include ${gmock_SOURCE_DIR})
add_subdirectory(stateTests)

add_executable(designpatternscsharpopen2 ${SOURCE_FILES})