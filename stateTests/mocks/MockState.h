//
// Created by Zoltan Krizsan on 2017. 03. 24..
//

#ifndef DESIGNPATTERNSCSHARPOPEN2_MOCKSTATE_H
#define DESIGNPATTERNSCSHARPOPEN2_MOCKSTATE_H


#include "../../state/IState.h"
#include <gmock/gmock.h>

class MockState : public IState{
public:
    MOCK_METHOD0(play, void());
    MOCK_METHOD0(stop, void());
    MOCK_METHOD0(backward10Frame, void());
    MOCK_METHOD0(forward10Frame, void());
    MOCK_METHOD0(pause, void());
    MOCK_METHOD0(spin, void());
    MOCK_METHOD0(toString, string());
};


#endif //DESIGNPATTERNSCSHARPOPEN2_MOCKSTATE_H
