//
// Created by Zoltan Krizsan on 2017. 03. 24..
//

#include <gtest/gtest.h>
#include "../state/DvdPlayerContext.h"
#include "mocks/MockState.h"

TEST(DvdPlayerContextTest, play){

    MockState mockState;
    EXPECT_CALL(mockState, play());
    DvdPlayerContext dvdPlayerContext(& mockState);
    dvdPlayerContext.play();

}