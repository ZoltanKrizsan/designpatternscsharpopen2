# Stratégia
 
## Termék - ár 

Hozz létre egy `IRoundStrategy` interfészt, melyben csak 1 metódsu van:

* `int Convert(double from)`

Hozz létre egy `Product` osztályt, 

* mely tárolja a termék árát (`price`, `int` típusú), és a kerekítést megvalósító 
interfész referenciáját (`IRoundStrategy`)!
* Hozz létre egy konstruktort mellyel az attribútumok beállíthatóak.
* Definiálj egy csak olvasható tulajdonságot a termék árának kinyerésére (`Price`).
* Hozz létre egy áremelést (`Increase`) megvalósító metódust. Ennek paramétere a az áremelés mértéke
százalékban, visszatérési értéke pedig a számolt egész érték.
`public int Increase(double percent)`
Mivel az új érték nem biztos, hogy egész lesz, ezért kerekíteni kell.
Használjuk az attribútumban tárolt kerekítő objektum `Round` metódusát!

Hozz létre két kerekítő implementációt: egészre, ötösre kerekítőt.

