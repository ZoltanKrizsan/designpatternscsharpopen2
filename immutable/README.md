# Immutable

## Ellenőrző kérdések

* Hogyan kell egy attribútum változását megakadályozni?
* A readonly tagot csak korlátozottan lehet módosítani. Hol lehetnek `readonly` attribútumot módosító kódok?

## Bolygók felfedezése

Hozz létre egy `Planet` nevű osztályt, amelynek állapota nem változhat! Most csak olyan attribútumok vannak,
amelyek nem változnak. 

* A Bolygót, amikor felfedezik, akkor meghatározzák az átmérőjét, és el is nevezik (tároljuk azeket a `_radius`, `_name` attribútumokba). (A bolygó életében ezek az adatok nem fognak változni.)
* Hozz létre egy konstruktort, amelyben az objektum megkapja a két adatot kívülről, és rendre beállítja azokat!
* Hozz létre tulajdonságokat az adattagokra!

Szükség van még egy `EquatorialCoordinate` osztályra is, amely azt tárolja, hogy hol látjuk a bolygót (relatív adat). 

* Két `long` típusú attribútummal rendelkezik: `_longitude`, `_azimuth`.
(Ezek az adatok sem változnak egy rögzített origójú rendszerben. Ha a megfigyelt entitás változik, pl. bolygó
kering a nap körül, akkor nem a koordinátájának az értéke változik, hanem egy másik koordinátára kerül.) Legyen ezért a két attribútuma `readonly`! 
* Hozzunk létre egy konstruktort, amely megkap két `long` típust, és beállítja az attribútumok 
értékét! 
* Hozzunk létre tulajdonságokat az attribútumokra!

Hozz létre egy interfészt `IPlanetCalculator`, amely segít meghatározni egy adott helyen levő bolygó átmérőjét!

* Egy long `GetPlanetRadiusInPosition(Point point)` metódust tartalmaz! Ennek az implementációját majd megadják a 
felhasználás során (a valóságban is más megvalósítása volt 300 éve és más most), neked csak használni kell.

Hozz létre egy osztály amely tárolja a bolygó abszolút koordinátáit (`Point`). A bolygó abszolút helyzetét írja le.

* Ennek az osztálynak három attribútuma van,  `_x`, `_y`, `_z`. 
* Hozzunk létre egy konstruktort, amely megkap három `long` típust, és beállítja az attribútumok értékét! 
* Hozzunk létre csak olvasható tulajdonságokat az attribútumokra!

Hozz létre egy interfészt, amely a nemzetközi csillagász szövetség szolgáltatásait definiálja `IInternationalActronomicalUnion` néven, 
amelyek legyenek a következök:

```
bool IsThePlanetDiscovered(DateTime noticeDate, EquatorialCoordinate equatorialCoordinate)
```

Visszaadja, hogy az adott pillanatban adott helyen látszó bolygót felfedezték e már (igaz, ha már ismert)

```
Point GetRealPosition(DateTime noticeDate, EquatorialCoordinate equatorialCoordinate)
```

Visszaadja az abszolút koordinátát a viszonylagos koordináta függvényében.

```
bool Register(Planet planet, Point position)
```

Beregisztrálja az új bolygót. Hamissal tér vissza, ha a regisztrációt nem sikerült. (Már felfedezték, vagy egyéb akadálya van.)
(Az implementációt, majd megadják később.)

Végül hozzunk létre egy központi csillagász osztályt (`Astronomer`), amely az előzőeket használja! 

* Minden csillagásznak van neve, 
nyilvántartja hogy milyen bolygókat fedezett fel (`List<Planet>` típusú). 
Működéséhez szükség van továbbá két külső entitásra: a már létező `IInternationalActronomicalUnion` és a `IPlanetCalculator` 
egy egy példányára (4 attribútum). 
* Szükség van egy konstruktorra, amellyel az attribútumok beállíthatóak (név + két függőség). 
* Szükség van egy csak olvasható tulajdonságra, amely visszaadja a felfedezett bolygókat (`DiscoveredPLanets`).

A csillagász figyeli az égboltot. Adott pillanatban, adott helyet figyeli, amelyet a következő metódus szimulál:

```
public bool ObserveTheSky(DateTime date, EquatorialCoordinate equatorialCoordinate)`
```

Ez a metódus lekérdezi a nemzetközi szövetséget, hogy az adott helyen látszó valamit felfedezték-e már (`IsThePlanetDiscovered`).
Ha még nem, akkor lekérdezi a szövetségtől a valódi abszolút koordinátát (`GetRealPosition`), majd kiszámíttatja az adott 
koordinátán levő égitest átmérőjét (`GetPlanetRadiusInPosition`). 
Az új adatok birtokában beregisztrálja az új égitestet, használva a saját nevét (magáról nevezi el).

Ha a regisztráció sikeres volt, akkor eltárolja az új bolygót a saját listáján.

Hamissal tér vissza, ha új bolygót sikerült felfedeznie, de a nemzetközi regisztráció sikertelen volt (minden más eset sikeres).

## Bónusz feladat 1.

Hozz létre egy factory-t a bolygók létrehozására, amely `Create` metódusa megkapja a
bolygó nevét, és visszatér az adott bolygó példányával.



