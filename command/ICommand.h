//
// Created by Zoltan Krizsan on 2017. 03. 24..
//

#ifndef COMMAND_ICOMMAND_H
#define COMMAND_ICOMMAND_H


class ICommand {

public:
    void execute();
    void unexecute();
    virtual ~ICommand();
    ICommand();
};


#endif //COMMAND_ICOMMAND_H
