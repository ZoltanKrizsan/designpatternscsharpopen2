# abstract factory tervezési minta

## Ellenőrző kérdések

* Hogy biztosítja a minta azt, hogy ne kelljen a kliens módosítani egy új termék
bevezetése esetén?
* A létrehozó metódusok miért nem `static` minősítésűek?
* Mi a különbség a factory és az abstract factory között?

## Kontinensekre jellemző kapcsolatban levő állatok létrehozása 

Állatok: növényevők, ragadozók. Kapcsolat az állatok között: A ragadozók megeszik a növényevőket.
Minden kontinensen kialakultak ezek a kategóriák, de mások. Egy kliens szempontjából lényegtelen, hogy milyen konkrét ragazodó van
csak az interfészét ismeri, és tudja használni.

Hozzunk létre egy `IHerbivore` interfészt. 

Hozzunk létre egy `ICarnivore` interfészt, melyben csak 1 `eat` metódus van, paramétere egy `IHerbivore` példány. 

Hozzunk létre egy `IAbstractAnimalFactory` interfészt, melyben két metódus van:
 
 * `ICarnivore *createCarnivore();`
 * `IHerbivore *createHerbivore();`

Hozz létre egy névteret `america` névvel.
Ebben a névtérben 

* hozz létre egy `Bison` osztályt, ami implementálja a `IHerbivore` interfészt.
* hozz létre egy `Wolf` osztályt, ami implementálja a `ICarnivore` interfészt.
* hozz létre egy `AmericaAnimalFactory` osztályt, ami implementálja a `IAbstractAnimalFactory` interfészt.
  * a `CreateCarnivore` visszaad egy `Wolf` pointert.
  * a `CreateHerbivore` visszaad egy `Bison` pointert.

Hozz létre egy névteret `africa` névvel.
Ebben a névtérben 

* hozz létre egy `Wildebeest` osztályt, ami implementálja a `IHerbivore` interfészt.
* hozz létre egy `Lion` osztályt, ami implementálja a `ICarnivore` interfészt.
* hozz létre egy `AfricaAnimalFactory` osztályt, ami implementálja a `IAbstractAnimalFactory` interfészt.
  * a `CreateCarnivore` visszaad egy `Lion` pointert.
  * a `CreateHerbivore` visszaad egy `Wildebeest` pointert.

Hozz létre egy `CameraMan` osztályt, aminek függősége a `IAbstractAnimalFactory` interfész egy pointere.

* Hozz létre attribútumot és konstruktort a függőség kezelésére.
* Hozz létre egy `createMovie` metódust (paraméter nélküli). A törzsében használva a beinjektált
factoryt, létrehoz egy növényevőt, majd egy ragadozót, és meghívja az `eat` metódust.



