# Adapter

## játék 

Hozz létre egy `IItem` interfészt, melyben most csak 

* `string getCaption()` metodus van.

Hozz létre egy `IRepresentation` interfészt, melyben

* `void setCaption(string itemName)`

Minden `IItem` implementáció a függőségeként megkapott IRepresentation-t
használva állítja elő a feliratát, csak mást ad át annak.

##Reprezentációk

Implementáld a `IRepresentation` interfészt egy `NormalRepresentation` osztályban.
A metódusban térj vissza a kapott paraméter kisbetűs verziójával.

Implementáld a `IRepresentation` interfészt egy `VisuallyImparedRepresentation` osztályban.
A metódusban térj vissza a kapott paraméter NAGYBETŰS verziójával.

## Játék elemek

Hozz létre egy `Settlement` osztályt, amely implementálja az `IItem` interfészt.
Eltárolja a település nevét, melyre van setter.
Függősége a IRepresentation címe (attribútum + konstruktor).

* a `getCaption` metódus használja a `IRepresentation`-t, paraméterének a település nevét adja.

Hozz létre egy `Character` osztályt, amely implementálja az `IItem` interfészt.
Eltárolja a karakter nevét, és a típusát.
Függősége a `IRepresentation` címét (attribútum + konstruktor).

* a `getCaption` metódus használja a `IRepresentation`-t, paraméterének a karakter nevét és típusát adja.
(pl.: "Logan (warrior)")

A karakter típusára vezess be egy CharacterType felsorolt típust (Warrior, Wizard).