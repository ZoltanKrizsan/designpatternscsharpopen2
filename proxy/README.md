# proxy

## Egy kötegelve loggoló proxy osztály

Hozz létre egy `ILog` interfészt

* LogRequest metódus (string paraméter)


Hozz létre egy `IRealLogger` interfészt, amely a `ILog` interfészből származik, de egy uj metódust is deklarál
`void ChangeLogLevel();`


Hozz létre egy `LogProxy` osztályt, ami implementálja az `ILog` interfészt
a loggolandó üzeneteket a `_logMessages`, a tényleges loggolo objektum referenciáját
 pedig a readonly `_logger` attribútumba tárolja.
 
* Definiálj egy `PageSize` konstanst 10-es értékkel.
* definiálj egy konstruktort, amelyben magkapod a valós loggoló objektumot, és elmented.
* `AddLogging` (private) : paramétere a log üzenenet, amit tárol a listában.
* `PerformLogging` (private): meghívja a valóságos loggoló fv-ét a `RealLogger`-nek szükség esetén.
* `LogRequest` metódus az implementációja az interfészben előírt metódusnak. Nem adja tovább a kérést közvetlen, hanem
  * csak minden PageSize-adik alkalommal (most 10) hívja a `PerformLogging` metódust.
  * ha nem kell ténylegesen loggolni, akkor gyűjti a log üzeneteket a listába, azaz
  meghívja az `AddLogging` metódust



