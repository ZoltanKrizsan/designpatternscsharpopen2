﻿# Adapter

Adott egy `IFileManagerUtil` interfész, ami koncepció idegen,

Feladat írni egy `FileManagerAdapter`-t, ami átalakítja a felületet
`IFileManagerUtil`-ról `IFileManager`-re.
Az adapter tárolja a `IFileManagerUtil` egy referenciáját, és minden kérést a `IFileManagerUtil`
referenciájának delegál tovább, elvégezve a megfelelő konverziókat paraméteren, és visszatérési értéken.

```
 class IFileManagerUtil
    {
        bool openFile(string filename) = 0;
        bool closeFile() = 0;
        bool writeToFile(string text, int offset, int length) = 0;
        rtring readFromFile(int offset, int length) = 0;

    }
```

```
 class IFileManager
    {
        Status open(string filename) = 0;
        Status close() = 0;
        Status read(int offset, int length, char[] bytes) = 0;
        Status write(int offset, int length, char[] bytes) = 0;     
    }
```

```
 enum Status
    {
        Success, Failed

    }
```