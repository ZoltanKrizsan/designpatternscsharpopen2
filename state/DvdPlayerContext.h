//
// Created by Zoltan Krizsan on 2017. 03. 24..
//

#ifndef DESIGNPATTERNSCSHARPOPEN2_DVDPLAYERCONTEXT_H
#define DESIGNPATTERNSCSHARPOPEN2_DVDPLAYERCONTEXT_H


#include "IState.h"

class DvdPlayerContext {

public:
    DvdPlayerContext(IState *state);

    void play();
private:
    IState *_state;
};


#endif //DESIGNPATTERNSCSHARPOPEN2_DVDPLAYERCONTEXT_H
