# Állapot
 
## DVD lejátszó

Definiáld a `IState` absztrakt osztályt. A lejátszó viselkedésit deklarálja:

*	`public abstract void Play();`
*	`public abstract void Stop();`
*	`public abstract void Backward10Frame();`
*	`public abstract void Forward10Frame();`
*	`public abstract void Pause();`
*	`public abstract void Spin();`
*	`public abstract String ToString();`

Valamint ezen felül lehetővé teszi, hogy állapotból is lehessen állapotot váltani, ezért
eltárolja függőségként a `DvdPlayerContext` példányát + konstruktor.
A leszármazott konkrét állapot osztályoknak elérhetővé teszi a tárolt kontextust.

### Konkrét állapot osztályok

Hozz létre egy `PlayingState` osztályt, amely a `State` osztályból származik.
Konstruktora megkapja a kontextust, amivel inicilizálja az ős konstruktorát.
Elvárt működés

*	`Play`: nem csinál semmit
*	`Stop`: A kontextusban lecseréli az állapotot a `StoppedState`-re, a poziciót 0-ra állítja.
*	`Backward10Frame`: nem támogatott, kivételt dob
*	`Forward10Frame`: nem támogatott, kivételt dob
*	`Pause`: A kontextusban lecseréli az állapotot `StoppedState`-re
*	`Spin`: eggyel növeli a poziciót
*	`ToString`: visszaadja az állapot nevét

Hozz létre egy `StoppedState` osztályt, amely a `State` osztályból származik.
Konstruktora megkapja a kontextust, amivel inicilizálja az ős konstruktorát.
Elvárt működés
*	`Play`: A kontextusban lecseréli az állapotot `PlayingState`-re.
*	`Stop`: poziciót nulláz
*	`Backward10Frame`: poziciót 10 értékkel visszább
*	`Forward10Frame`: poziciót 10 értékkel előrébb
*	`Pause`: nem csinál semmit
*	`Spin`: nem csinál semmit
*	`ToString`: visszaadja az állapot nevét

Hozz létre egy `DvdPlayerContext` osztályt. Ez a nyilvános felülete mintának, ezt látják a kliensek.

* attribútumai
  * postion (ahol a lejátszás áll)
  * length (film hossza)
  * state (egy absztrakt State osztály típusú)
* generálj tulajdonságokat az attribútumokra.
* konstruktorban állítsa be a `StoppedState`-re az állapotot.
* tartlamaz metódusokat, melyek a viselkedésekért felelősek, azokat delegálja az aktuális állapot metódusainak.