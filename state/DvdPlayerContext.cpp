//
// Created by Zoltan Krizsan on 2017. 03. 24..
//

#include "DvdPlayerContext.h"

void DvdPlayerContext::play() {
    _state->play();
}

DvdPlayerContext::DvdPlayerContext(IState *state) {
    _state = state;

}
