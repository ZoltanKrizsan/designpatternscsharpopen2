//
// Created by Zoltan Krizsan on 2017. 03. 24..
//

#ifndef STATE_ISTATE_H
#define STATE_ISTATE_H

#include <string>

using namespace std;

class IState {
public:
    virtual void play() = 0;

    virtual void stop() = 0;

    virtual void backward10Frame() = 0;

    virtual void forward10Frame() = 0;

    virtual void pause() = 0;

    virtual void spin() = 0;

    virtual string toString() = 0;
};


#endif //STATE_ISTATE_H
