# Observer

Egy roboton belül együttműködő rendszert fogunk szimulálni. A robotot mozgató (`Rower`) és a robot karját emelő (`Arm`) 
egységek csak akor működhetnek, ha biztonságos. Ahalyett, hogy ezek az egységek folyamatosan lekérdeznék a távolság 
érzékelő egységtől az adott értéket, feliratkoznak, mint megfigyelők, és amikor változás következik be,
akkor értesítést kapnak. Holywood elv alapján működik. (álló robot esetében nem árasztják el a rendszert felesleges 
lekérdezéssel)

Hozz létre két interfészt:

* `IDistanceObserver`
  * előírja az értesítés módját, paraméterét: `void Update(int distance);`
* `IDistanceSensor`
  * metódusokat deklarál a fel és leiratkozásra
    * `void Register(IDistanceObserver distanceObserver);`
  	* `void Unregister(IDistanceObserver distanceObserver);`
  	
Hozz létre egy `DistanceSersor` osztályt, ami implementálja a `IDistanceSensor` interfészt.

* Amely nyilvántartja a megfigyelőket
  * privát `_distanceObservers` listában tárolja (`IDistanceObserver`-ek)
  * `Register` eltárolja a listában a megfigyelőt  
  * `Unregister` törli a listából a megfigyelőt
* Értesíti a megfigyelőket a változásról
  *  publikus `SimulateData(int data)`, értesíti a megfigyelőket. 
  (végigjárja a `_distanceObservers` listán és hívja a `IDistanceObserver`-ek `Update` metódusát.)
  
Hozz létre egy `Arm` osztályt, amely az `IDistanceObserver` interfészt implementálja.

* legyen egy `_blocked` adattagja, amely ha igaz, akkor az egység működik.
* definiálj csak olvasható tulajdonságot a `_blocked` attribútumra.
* szükség van egy `MinimumDistance` readonly attribútumra is, aminek értéke 350.
* konstruktor 
  * megkapja a `IDistanceSensor` függőségét, és eltárolja egy privát attribútumban.
  * beregisztrálja az eszközt a Senzorra.
  * beállítja a `MinimumDistance` 350-re
* implementálja az interfész metódusát ugy, hogy
  * ha a kapott távolság kisebb, mint a `MinimumDistance`, akkor letiltja az eszközt.
* `SwitchOff` metódus, ami kikapcsolja az egységet
  * blokkoltá teszi, és
  * törli a regisztrációt a szenzornál.
* `Lift` metódus a valósában mozgatná a kart, ha nincs tiltva. Itt csak visszatér, hogy sikerült-e a művelet.
(sikerült, ha nincs tiltva)

Hozz létre egy `Rower` osztályt, amely az `IDistanceObserver` interfészt implementálja.

* legyen egy `_blocked` (igaz, akkor az egység működik) és egy `_movingForward` adattagja (igaz, akkor az egység mozog)
* definiálj csak olvasható tulajdonságokat az attribútumokra.
* szükség van egy `MinimumDistance` final attribútumra is, aminek értéke 650.
* konstruktor 
  * megkapja a `IDistanceSensor` függőségét, és eltárolja egy privát attribútumban.
  * beregisztrálja az eszközt a Senzorra.
  * beállítja a `MinimumDistance` 650-re
* szükség van egy `EmergencyStop` privát metódusra, amely most csak 
* implementálja az interfész metódusát ugy, hogy
  * ha a kapott távolság kisebb, mint a `MinimumDistance`, akkor letiltja az eszközt.
  * Ha túl közel van, akkor meghívja az `EmergencyStop` metódust.
* `SwitchOff` metódus, ami kikapcsolja az egységet
  * blokkoltá teszi, és
  * törli a regisztrációt a szenzornál.
* `MoveForward` valósában mozgatná a robot, ha nincs tiltva. Itt csak visszatér, hogy sikerült-e a művelet, és bebillenti 
a `_movingForward` attribútumot. (sikerült, ha nincs tiltva)

Érdemes lenne bevezetni egy `Actuator` osztályt, mert az `Arm` és `Rower` osztályok viselkedése, 
működése nagyon hasonló.
