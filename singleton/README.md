# Singleton

## Raktár, termelő, fogyasztó szimuláció

Hozz létre egy termék (`Product`) oszályt,  

* amelynek van neve (attribútum),   
* van konstruktora, amely beállítja a nevét,  
* és getter a nevére.

Hozz létre egy interfészt `IStore` azonosítóval, a következő elemekkkel  

* `add` metódus, amely bead egy terméket a raktárba. 
`StoreFullException` dobódik, ha már nem fér be a raktárba  
* `remove` metódus, amely kivesz egy terméket a raktárból
`StoreEmptyException` kivétel dobódik, ha a raktár már üres.  
* `reset` metódus, ami törli a raktár tartalmát.  
* `productCount` getter, ami visszaadja a raktárban levő termékek számát.

Implementáld a `IStore` interfészt a `Store` osztályban úgy, hogy
A Store-ből csak 1 jöhet létre a rendszerben!
A termékek tárolására vezess be egy `_products` nevű attribútumot (Queue).
Vezess be egy `CAPACITY` konstanst 3000 értékkel.
Implementáld a megfelelő metódusokat értelemszerűen. (Ne feledkezz meg a két kivétel dobásáról sem.)

Hozz létre egy `Consumer` osztályt, melynek függősége a `IStore`.  

* Hozz létre benne egy `consume` metódust, ami megpróbál kivenni egy terméket a raktárból és visszaadja azt.
(kivételt nem kezeli.)

Hozz létre egy `Producer` osztályt, melynek függősége a `IStore`.  

* Hozz létre benne egy `produce` metódust, ami megpróbál létrehozni és betenni a raktárba egy terméket.
(kivételt nem kezeli.) A termék neve legyen product és a sorszáma. pl. product0, product1, ...

## Bonusz feladat
* Tedd a raktárat szálbiztossá.
* Készíts egy demo alkalmazást, ami véletlenszerűen létrehoz fogyasztókat, termelőket. Ezek külön szálban futnak,
. Álljon le az alkalmazás, ha kifogyott vagy megtelt a raktár.
* Hozz létre termék factory-kat, amelyek Toll, Laptop, Guminyul termekeket állítnak elő.